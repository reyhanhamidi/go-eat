require 'order/order_item'
require 'object/driver'
require 'object/store/store'
require 'map/map'
require 'json'

class Order
    attr_reader :store, :driver, :delivery_fee
    DELIVERY_COST = 300

    def initialize(user, store)
        @user = user
        @store = store
        @order_items = []
        @delivery_fee = delivery_fee
    end

    def set_driver(driver)
        if driver.is_a? Driver
            @driver = driver
        else
            raise "#{driver} is not a Driver!"
        end
    end

    def add_order_item(item, amount)
        if amount < 0
            raise "--- Order item amount can not be negative number ---"
        end

        order_item_id = @order_items.index { |o| o.item == item }
        if order_item_id
            # Update amount
            update_amount(order_item_id, amount)
        else
            # Create new order item
            @order_items.append(OrderItem.new(item, amount)) if amount > 0
        end            
    end

    def update_amount(order_item_id, amount)
        if amount == 0
            @order_items.delete_at order_item_id
        else
            @order_items[order_item_id].amount = amount
        end
    end

    def total
        total = 0
        @order_items.each do |order_item|
            total += order_item.subtotal
        end
        total + delivery_fee
    end

    def distance
        (@store.pos.r - @user.pos.r).abs \
            + (@store.pos.c - @user.pos.c).abs
    end

    def delivery_fee
        distance * DELIVERY_COST
    end

    def set_path(path_to_store, path_to_user)
        if (path_to_store.is_a? Map) && (path_to_user.is_a? Map)
            @path_to_store = path_to_store
            @path_to_user = path_to_user
        else
            raise "Path should be a Map"
        end
    end

    def to_json
        hash = {}
        hash["driver"] = @driver.name
        hash["store"] = @store.name
        hash["order_item"] = []
        @order_items.each do |order_item|
            each_order_item = {}
            each_order_item["item"] = order_item.item.name
            each_order_item["price"] = order_item.item.price
            each_order_item["amount"] = order_item.amount
            each_order_item["subtotal"] = order_item.subtotal
            hash["order_item"].append(each_order_item)
        end
        hash["delivery_fee"] = @delivery_fee
        hash["total"] = total
        hash["distance"] = distance
        hash["path_to_store"] = @path_to_store.to_s.split("\n")
        hash["path_to_user"] = @path_to_user.to_s.split("\n")
        JSON.pretty_generate(hash)
    end

    def to_s
        output = "#{"-"*5} Order on #{@store.name} #{"-"*5}\n"
        index = 0
        @order_items.each do |order_item|
            output += "*) #{order_item}\n"
        end
        output += "*) Delivery Fee #{" "*4} = #{@delivery_fee}\n"
        output += "#{"-"*14} Total = #{total}"
        output
    end
end
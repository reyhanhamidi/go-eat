class OrderItem
    attr_reader :item, :amount

    def initialize(item, amount)
        @item = item
        @amount = amount
    end

    def subtotal
        @item.price * @amount
    end

    def amount=(amount)
        if amount < 0
            raise "The order item amount must not be negative number!"
        else
            @amount = amount
        end
    end

    def to_s
        "#{@item} * #{amount} = #{subtotal}"
    end
end
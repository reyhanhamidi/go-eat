require 'map/map'
require 'controller/go_eat'

# A module that help main class to handle user argument
module ArgumentHelper
    def create_go_eat
        if ARGV.length == 0
            empty_argument
        elsif ARGV.length == 1
            file_argument
        elsif ARGV.length == 3
            size_and_pos_argument
        else
            puts "--- The argument is invalid ---\n" + \
            "Format: [map_size] [user_row_position] [user_col_position]\n" + \
            "*without bracket"
        end
    end

    def empty_argument
        GoEat.new
    end

    def file_argument
        filename = ARGV[0]
        data = JSON.parse(File.read(filename))
        GoEat.new(data)
    end

    def size_and_pos_argument
        begin
            size = Integer(ARGV[0])
            user_row = Integer(ARGV[1])
            user_col = Integer(ARGV[2])        
        rescue ArgumentError => exception
            puts "=== Please insert correct argument in integer ==="
        else
            if size > 3
                GoEat.new(size, user_row, user_col)
            else
                puts "=== The size of the map is too small! ==="
            end
        end        
    end
end
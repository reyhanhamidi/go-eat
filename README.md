# Go-Eat

## Description

A simple Go-Food-like CLI program in Ruby. This program made for Second Stage Assignment for GOJEK x Compfest 11 Software Engineering Academy.

> See the complete [specification](https://docs.google.com/document/d/1C5EUZJjNoWGRfgnFb1zD78eWQciSqRs-woFG1vYNT04/edit)

## Assumption

- Driver can drive through other object (user, store, or other driver)

## Design Decision

### Object

- `User`, `Driver`, `Store` class created as to represent the main objects that interact in this app.
- Those three class has `Position` attribute to determine the position (row, column) of the object in the map
- Composition used instead of using inheritance so the object can be more flexible and independent from other class.

> "Composition over inheritance"
> -- most Design Pattern book said

- `Menu` created to separate class (instead of only a hash map) to represent menu on a store so when new specification added (maybe discount menu, limited menu, etc) this class can easily **extend its behaviour** without changing the existing class
> Open-Closed Principle
> "Open for extension but closed for modifications"

### Map

- `Map` class stored the 2D array map and some method that dealing with that 2D array map
- `Road` used to define an empty cell in map
- `Path` used to define the path taken by the driver
- `Char` module used to store the display character of the object on the 2D map

### Order
- `Order` holds the order data that would be stored as order history
- `Order_Item` created as separate class to represent the menu ordered and its amount. Like `Menu`, a simple hash map stored in `Order` class would help too. But to achieve **Single Responsibility Principle**, this is necessary especially when things got more complicated in the future.

### Controller

- `Go-Eat` class used to handle the back-end and store the data of the Go-Eat app
- There's several helper module contains method needed by Go-Eat to handle different task
- `Go-Eat` split to several module to achieve **Separation of Concern**

### Main class

- `Main` class helped by argument_helper to simplify main class.
- `Main` class create a `Go-Eat` object through a method `create_go_eat()` that handle the different argument so `Main` class don't have to deal with the complexity of it. `Main` class depend on the **abstraction** of Go-Eat creation so if the argument specification change, the main class wouldn't change.

> Dependency Inversion Principle
> -- The D on SOLID
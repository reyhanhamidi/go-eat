class Position
    attr_reader :r, :c
    
    def initialize(row = nil, col = nil)
        @r = row
        @c = col
    end

    def set_position(row, col)
        @r = row
        @c = col
    end

    def to_s
        "(#{r}, #{c})"
    end
end
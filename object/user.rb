require 'module/char'

# A customer that order food from Go-Eat
class User
    attr_reader :name, :pos
    attr_accessor :char    
    
    def initialize(name)
        @name = name
        @pos = Position.new
        @char = Char::USER
    end

    def to_s
        "#{@name} at (#{@pos.r}, #{@pos.c})"
    end
end
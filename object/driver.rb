require 'module/char'

# A Go-Eat driver that receive food delivery order from User
class Driver
    attr_reader :name, :pos, :rating
    attr_accessor :char
    
    def initialize(name)
        @name = name
        @pos = Position.new
        @char = Char::DRIVER
        @rating = 0.0
        @num_of_trip = 0
    end

    # Add a new rating and update its average
    def add_rating(new_rating)
        if new_rating.between?(1,5)
            rating_sum = (@rating * @num_of_trip) + new_rating
            @num_of_trip += 1
            @rating = rating_sum / @num_of_trip
        else
            raise "Rating must be an integer in range 1 to 5"
        end
    end

    def to_s
        "#{@name} at (#{@pos.r}, #{@pos.c})"
    end
end
require 'module/char'
require 'object/position'
require 'object/store/menu'

# A food store that serve food order from Driver
class Store
    attr_reader :name, :pos, :menus
    attr_accessor :char    
    DEFAULT_MIN_MENU = 2
    DEFAULT_MAX_MENU = 5
    DEFAULT_MIN_PRICE = 10
    DEFAULT_MAX_PRICE = 50

    
    def initialize(name, menu_data = [])
        @name = name
        @pos = Position.new
        @char = Char::STORE
        @menus = []
        if menu_data.empty?
            generate_random_menu
        else
            generate_menu(menu_data)
        end
    end

    # Generate menu from hash
    def generate_menu(menu_data)
        menu_data.each do |menu|
            puts menu.class
            puts menu["name"]
            puts menu["price"]
           @menus.append(Menu.new(menu["name"], menu["price"]))
        end
        @menus
    end

    def add_menu(menu_list)
        menu_list.each do |menu|
            @menus.append(menu)
        end
    end

    def generate_random_menu
        @menus = []
        num_of_menu = rand(DEFAULT_MIN_MENU..DEFAULT_MAX_MENU)
        num_of_menu.times do |index|
            price = rand(DEFAULT_MIN_PRICE..DEFAULT_MAX_PRICE) * 1000
            @menus.append(Menu.new("Menu #{index + 1}", price))
        end
    end

    # Print Store name, position and all of its menu
    def to_s
        output = "#{"-"*5} #{@name} #{"-"*5}\n"
        index = 0
        @menus.each do |menu|
            index += 1
            output += "#{index}. #{menu}\n"
        end
        output
    end
end
# A menu that can be ordered by User from Store

class Menu
    attr_reader :name, :price
    def initialize(name, price)
        @name = name
        @price = price
    end

    def to_s
        "#{@name} @#{@price}"
    end
end
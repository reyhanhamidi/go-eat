# A module that help to use method gets no matter what the file argument is
module InputHelper
    # Handle input method based on its file argument
    def gets
        if ARGV.nil?
            Kernel.gets
        else
            STDIN.gets
        end
    end
end
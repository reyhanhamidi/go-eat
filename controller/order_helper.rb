require 'json'
require 'time'

# A module that handle order food stuff on Go-Eat 
module OrderHelper
    TIME_FORMAT = "%d-%m-%Y_%H:%M:%S"
    JSON_FILE_FORMAT = "file/order_history/order_history_#{TIME_FORMAT}.json"
    INVALID_INPUT_MSG = "--- Please insert correct %s number ---"

    # Display Order Food Page
    def order_food
        loop do 
            display_store_list(@stores)
            print "Choose store: "
            begin
            input = Integer gets
            break if input == 0
                process_store_input(input)
            rescue ArgumentError => exception
                puts INVALID_INPUT_MSG % "store"
            rescue => exception
                puts exception
            end
        end
    end

    # Display store if the input is correct
    def process_store_input(store_input)
        store = get_store(store_input)
        @active_order = Order.new(@user, store)
        loop do
            break if @active_order.nil?
            display_store_menu(store)            
            print "Choose menu: "
            begin
            menu_input = Integer gets
            break if menu_input == 0
                process_menu_input(store_input, menu_input)
            rescue ArgumentError => exception
                puts INVALID_INPUT_MSG % "menu"
            rescue => exception
                puts exception
            end
        end
    end

    # Return store based on its index number
    def get_store(id)
        if id < 1
            raise INVALID_INPUT_MSG % "store"
        end

        store = @stores[id-1]
        if store
            store            
        else
            raise INVALID_INPUT_MSG % "store"
        end
    end

    # Ask user to set amount of the order menu
    def process_menu_input(store_input, menu_input)
        store = @stores[store_input-1]
        
        if menu_input == order_food_num(store)
            process_order
        elsif menu_input > store.menus.length
            raise INVALID_INPUT_MSG % "menu"
        elsif menu_input > 0
            menu = store.menus[menu_input-1]
            begin
            print "Set amount: "
                amount = Integer gets
                @active_order.add_order_item(menu, amount)
                puts
                puts @active_order
            rescue ArgumentError => exception
                puts INVALID_INPUT_MSG % "amount"
                retry
            rescue => exception
                puts exception
                retry
            end
        else
            raise INVALID_INPUT_MSG % "menu"
        end
    end

    # Process the active order made by user
    def process_order
        store = @active_order.store

        # Finding Driver
        puts "\n--- Finding nearest driver from " \
        + "#{store.name} at #{store.pos} ---"
        nearest_driver = find_driver(store)
        @active_order.set_driver(nearest_driver)
        puts "--- DRIVER FOUND! ---"
        puts "Your driver is #{nearest_driver}"

        # Find path from driver to store
        path_to_store = find_path(nearest_driver, store)
        puts "\n--- DRIVER IS GOING TO THE STORE ---"
        puts path_to_store

        # Find path from store to user
        path_to_user = find_path(store, @user)
        puts "\n--- DRIVER IS COMING TO YOUR WAY ---"
        puts path_to_user

        @active_order.set_path(path_to_store, path_to_user)
        finish_order
        ask_to_rate(nearest_driver)
    end

    # Add active order to order history list
    def finish_order
        save_to_json(@active_order)
        @orders.append(@active_order)
        @active_order = nil
        puts "--- YOUR FOOD ARRIVED ---"
    end

    # Write order history to json file
    def save_to_json(order)
        time = Time.now
        json_file_name = time.strftime(JSON_FILE_FORMAT)
        File.open(json_file_name, "w+") { |f| f.write(order.to_json) }
    end

    # Ask user to rate the driver and evaluate the driver performance
    def ask_to_rate(driver)
        begin
            print "Rate your driver (#{driver.name}) [1-5 star]: "
            rating = Integer gets
            driver.add_rating(rating)
            evaluate_driver(driver)
        rescue ArgumentError => exception
            puts INVALID_INPUT_MSG % "rating"
            retry
        rescue => exception
            puts exception
            retry
        end
    end

    # Remove driver if his/her rating lower than 3.0
    def evaluate_driver(driver)
        remove_driver(driver) if driver.rating < 3.0
    end
end
# A module that help Go-Eat handle initialize and generate its data
module InitHelper
    DEFAULT_NUM_OF_DRIVER = 5
    DEFAULT_NUM_OF_STORE = 3

    # Initialize Go-Eat app when empty or three argument given
    def init_default(map_size = 20, *user_position)
        @map = Map.new(map_size)
        @user = create_user(user_position)
        @drivers = generate(Driver, DEFAULT_NUM_OF_DRIVER)
        @stores = generate(Store, DEFAULT_NUM_OF_STORE)
        save_data_to_json ########
    end

    # Initialize Go-Eat app when filename argument given
    # Create all data from the given hash map
    def init_from_hash(data)
        @map = Map.new(data["map_size"])
        @user = generate_from_hash(User, [data["user"]])[0]
        @drivers = generate_from_hash(Driver, data["drivers"])
        @stores = generate_from_hash(Store, data["stores"])
    end

    # Generate list of object from the data on given array of hash map
    def generate_from_hash(obj_class, arr_of_hash)
        return_list = []
        arr_of_hash.each do |each_hash|
            return_list.append(create_object_from_hash(obj_class, each_hash))
        end
        return_list
    end

    # Create a single object from the data on given hash map
    def create_object_from_hash(obj_class, obj_data)
        name = obj_data["name"]
        row = obj_data["row"]
        col = obj_data["col"]
        if obj_class == Store
            object = obj_class.new(name, obj_data["menus"])
        else
            object = obj_class.new(name)
        end
        @map.set_position(object, row, col)
        object
    end

    # Create object user
    # Set the position if given, randomize if otherwise
    def create_user(user_position)
        if user_position.empty?
            generate(User, 1)[0]
        elsif user_position.length == 2
            user = User.new("User 1")
            row = user_position[0]
            col = user_position[1]
            @map.set_position(user, row, col)
        else
            raise "User position argument expected: 2," \
            + "but given #{user_position.length}"
        end
    end

    # Generate numbers of object and place it on map randomly
    def generate(obj_class, num)
        return_list = []
        if [User, Driver, Store].include? obj_class
            num.times do |id_num|
                return_list.append(obj_class.new("#{obj_class} #{id_num + 1}"))
            end
        else
            raise "#{obj_class} Class unrecognized by the method"
        end
        @map.place_object(return_list)
        return_list
    end
end
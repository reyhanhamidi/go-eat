# A module contains method for displaying information or menu
module DisplayHelper
    WELCOME_TEXT = "#{"="*10} WELCOME TO GO-EAT #{"="*10}"
    BACK_ACTION = "0. BACK"

    # Display the main menu of the Go-Eat app
    def display_action_list
        puts "1. Show Map"
        puts "2. Order Food"
        puts "3. View History"
        puts "0. EXIT"
    end
    
    # Display store choice
    def display_store_list(stores)
        puts "\n#{"-"*5} STORE #{"-"*5}"
        index = 0
        @stores.each do |store|
            index += 1
            puts "#{index}. #{store.name} at #{store.pos}"
        end
        puts BACK_ACTION
    end

    def display_store_menu(store)
        puts
        puts store
        puts BACK_ACTION
        puts "#{order_food_num(store)}. < ORDER FOOD >"
    end

    # Return the number 99 if the number of menu 
    # on this store is less than 99
    def order_food_num(store)
        [store.menus.length + 1, 99].max
    end

    # Display order history
    def view_history
        order_id = 0
        @orders.each do |order|
            order_id += 1
            puts "\n#{"="*10} ORDER #{order_id} #{"="*10}"
            puts order
        end
    end
end
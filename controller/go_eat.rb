require 'object/driver'
require 'object/store/store'
require 'object/user'
require 'order/order'
require 'controller/display_helper'
require 'controller/map_helper'
require 'controller/order_helper'
require 'controller/input_helper'
require 'controller/init_helper'

# The controller of the Go-Eat app
class GoEat
    include DisplayHelper
    include MapHelper
    include OrderHelper
    include InputHelper
    include InitHelper

    def initialize(*args) #map_size = 20, *user_position)
        if args.length == 0
            init_default
        elsif args.length == 1
            init_from_hash(args[0])
        elsif args.length == 3
            init_default(args[0], args[1], args[2])
        else
            raise ArgumentError, \
                "Invalid number of arguments: #{args.length}"
        end

        @orders = []
        @active_order = nil
    end

    # Run the go-eat app
    def run
        puts @map
        input = nil
        while input != 0
            begin
                puts "\n" + WELCOME_TEXT
                display_action_list

                print "Choose action: "
                input = Integer gets
                case input
                when 0
                when 1 then show_map
                when 2 then order_food
                when 3 then view_history
                else raise ArgumentError
                end     
                
            rescue ArgumentError => exception
                puts INVALID_INPUT_MSG % "action"
            end
        end
    end
    
    # Save the all data to json so the data file
    # can be used again as argument to run Go-Eat app
    def save_data_to_json
        hash = {}

        hash["map_size"] = @map.map.size

        hash["user"] = {}
        hash["user"]["name"] = @user.name
        hash["user"]["row"] = @user.pos.r
        hash["user"]["col"] = @user.pos.c

        hash["num_of_driver"] = @drivers.length
        hash["drivers"] = []
        @drivers.each do |driver|
            each_driver = {}
            each_driver["name"] = driver.name
            each_driver["row"] = driver.pos.r
            each_driver["col"] = driver.pos.c
            hash["drivers"].append(each_driver)
        end

        hash["num_of_store"] = @stores.length
        hash["stores"] = []
        @stores.each do |store|
            each_store = {}
            each_store["name"] = store.name
            each_store["row"] = store.pos.r
            each_store["col"] = store.pos.c
            each_store["menus"] = []
            store.menus.each do |menu|
                each_menu = {}
                each_menu["name"] = menu.name
                each_menu["price"] = menu.price
                each_store["menus"].append(each_menu)
            end
            hash["stores"].append(each_store)
        end
        json_data = JSON.pretty_generate(hash)
        File.open("file/input/input1.json", "w+") { |f| f.write(json_data) }
    end
end
# A module to help Go-Eat to handle map and navigation stuff
module MapHelper
    # Show Go-Eat map
    def show_map
        puts
        puts "#{'-'*5} GO-EAT MAP #{'-'*5}"
        puts @map
    end

    # Find nearest driver from a store
    def find_driver(store)
        @drivers.min_by { |driver|
            ((driver.pos.r - store.pos.r).abs + (driver.pos.c - store.pos.c).abs)
        }
    end

    # Return a map with path from an object to other object
    def find_path(origin, destination)
        from = origin.pos
        to = destination.pos
        path_map = @map.dup
        # Deep copy current map
        path_map = Marshal.load(Marshal.dump(@map))
        path_map.draw_path(from, to)
        path_map
    end

    # Remove driver from driver list
    def remove_driver(driver)
        @drivers.delete(driver)
        @map.remove_object(driver)
        puts "--- #{driver.name} fired due to low rating ---"
        evaluate_num_of_driver
    end

    # Generate random driver if every driver is removed
    def evaluate_num_of_driver
        if @drivers.empty?
            puts "--- We have shortage of driver. Please wait... ---"
            num_of_driver = 5
            @drivers = generate(Driver, num_of_driver)
            puts "--- New #{num_of_driver} driver hired! ---"
        end
    end

end
require 'module/char'

# An path representation that passed by Driver
class Path
    attr_accessor :char
    def initialize
        @char = Char::PATH
    end
end
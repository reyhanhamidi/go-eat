require 'map/road'
require 'map/path'

class Map
    attr_reader :size
    attr_accessor :map

    def initialize(size = 20)
        @size = size
        make_empty_map
    end

    def make_empty_map
        # Initialize an empty map (actually it's filled by Road)
        @map = []
        @size.times do |r|
            row = []
            @size.times do |c|
                row.append(Road.new)
            end
            @map.append(row)
        end
    end

    # Place object on the map randomly
    def place_object(obj_list)
        obj_list.each do |object|
            # Look for empty coordinate
            row, col = 0, 0
            loop do
                row, col = rand(@size), rand(@size)
                break if @map[row][col].is_a? Road 
            end
            set_position(object, row, col)
        end
    end

    # Set object position on given coordinate
    def set_position(object, row, col)
        object.pos.set_position(row, col)
        @map[row][col] = object
    end

    # Replace the object in the map with a road
    def remove_object(object)
        row = object.pos.r
        col = object.pos.c
        @map[row][col] = Road.new
    end

    # Draw path from two given position
    # and highlight them by downcase its display character
    # - from = origin position
    # - to = destination position
    def draw_path(from, to)
        @map[from.r][from.c].char = @map[from.r][from.c].char.downcase
        @map[to.r][to.c].char = @map[to.r][to.c].char.downcase
        draw_path_rec(from, to)
    end

    # Draw path from two given position recursively
    def draw_path_rec(from, to)
        path_map = @map
        dist_row = to.r - from.r
        dist_col = to.c - from.c
        new_from_r = from.r
        new_from_c = from.c

        # Set the path direction        
        if dist_row.abs > dist_col.abs
            new_from_r = dist_row > 0 ? from.r + 1 : from.r - 1
        else
            new_from_c = dist_col > 0 ? from.c + 1 : from.c - 1
        end

        # Draw a path in a cell
        if to.r == new_from_r && to.c == new_from_c
            path_map
        else
            path_map[new_from_r][new_from_c] = Path.new
            new_from = Position.new(new_from_r, new_from_c)
            draw_path_rec(new_from, to)
        end
    end

    def to_s
        map_str = ''
        (@size + 2).times { map_str += Char::BORDER }
        map_str += "\n"

        @map.each do |row|
            map_str += Char::BORDER
            row.each do |cell|
                map_str += cell.char
            end
            map_str += Char::BORDER
            map_str += "\n"
        end

        (@size + 2).times { map_str += Char::BORDER }

        map_str
    end

end
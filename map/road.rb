require 'module/char'

# An empty road that can be passed by Driver
class Road
    attr_accessor :char
    def initialize
        @char = Char::ROAD
    end
end